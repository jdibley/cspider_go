package cspider

import "container/list"

type IntSeq struct {
	l *list.List
}

// NewIntSeq returns a sequence containing the supplied integers
func NewIntSeq(nums ...int) *IntSeq {
	rs := new(IntSeq).init()
	for _, num := range nums {
		rs.l.PushBack(num)
	}
	return rs
}

func (is *IntSeq) init() *IntSeq {
	is.l = list.New()
	return is
}

// AddBack adds the elements of the supplied sequence (in original order)
// to the back of the receiver's sequence.
func (is *IntSeq) AddBack(addSeq *IntSeq) *IntSeq {
	for num := addSeq.l.Front(); num != nil; num = num.Next() {
		is.l.PushBack(num.Value)
	}
	return is
}

// AddFront adds the elements of the supplied sequence (in original order)
// to the front of the receiver sequence.
func (is *IntSeq) AddFront(addSeq *IntSeq) *IntSeq {
	for num := addSeq.l.Back(); num != nil; num = num.Prev() {
		is.l.PushFront(num.Value)
	}
	return is
}

// AddIntBack adds a single int to the back of the receiver sequence
func (is *IntSeq) AddIntBack(i int) *IntSeq {
	is.l.PushBack(i)
	return is
}

// AddIntFront adds a single int to the front of the receiver sequence
func (is *IntSeq) AddIntFront(i int) *IntSeq {
	is.l.PushFront(i)
	return is
}

// AddRangeBack adds the supplied (inclusive) range of values to the
// back of the receiver's sequence.
func (is *IntSeq) AddRangeBack(sRange int, eRange int) *IntSeq {
	for i := sRange; i <= eRange; i++ {
		is.l.PushBack(i)
	}
	return is
}

// AddRangeFront adds the supplied (inclusive) range of values
// (preserving the original order) to the front of the receiver's sequence.
func (is *IntSeq) AddRangeFront(sRange int, eRange int) *IntSeq {
	for i := eRange; i >= sRange; i-- {
		is.l.PushFront(i)
	}
	return is
}

// Elem tests whether the supplied int is an element of the receiver.
func (is *IntSeq) Elem(x int) bool {
	found := false
	for e := is.l.Front(); e != nil; e = e.Next() {
		if e.Value.(int) == x {
			found = true
		}
	}
	return found
}

// Elements returns the values held by the receiver as a slice
func (is *IntSeq) Elements() []int {
	rsl := make([]int, 0)
	for num := is.l.Front(); num != nil; num = num.Next() {
		rsl = append(rsl, num.Value.(int))
	}
	return rsl
}

// Head returns the first element of the receiver _without removing it_.
func (is *IntSeq) Head() int {
	if is.Length() == 0 {
		return -1
	}
	e := is.l.Front()
	return e.Value.(int)
}

// Length returns the length of the receiver sequence.
func (is *IntSeq) Length() int {
	return is.l.Len()
}

// Null tests whether the receiver is empty.
func (is *IntSeq) Null() bool {
	return is.Length() == 0
}

// Remove deletes the elements of the supplied sequence from the receiver.
func (is *IntSeq) Remove(remSeq *IntSeq) *IntSeq {
	for rNum := remSeq.l.Front(); rNum != nil; rNum = rNum.Next() {
		for iNum := is.l.Front(); iNum != nil; iNum = iNum.Next() {
			if rNum.Value.(int) == iNum.Value.(int) {
				is.l.Remove(iNum)
			}
		}
	}
	return is
}

// Set returns an intSet containing the values held by the receiver.
func (is *IntSeq) Set() *IntSet {
	set := NewIntSet()
	for e := is.l.Front(); e != nil; e = e.Next() {
		set.AddInt(e.Value.(int))
	}
	return set
}

// Tail returns a new sequence consisting of the elements held by the receiver
// minus its head.
func (is *IntSeq) Tail() *IntSeq {
	rs := new(IntSeq).init()
	for e := is.l.Front().Next(); e != nil; e = e.Next() {
		rs.l.PushBack(e.Value)
	}
	return rs
}
