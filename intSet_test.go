package cspider

import (
	"fmt"
	"testing"
)

func TestNewIntSet(t *testing.T) {
	s := NewIntSet()
	if s == nil {
		t.Error(`NewIntSet() didn't return valid ptr`)
	}
	if s.Card() != 0 {
		t.Error(`NewIntSet() produced set with nonzero cardinality`)
	}

	v := NewIntSet(1, 3, 5, 7)
	if v == nil {
		t.Error(`NewIntSet(1,3,5,7) didn't return valid ptr`)
	}
	if v.Card() != 4 {
		t.Error(`NewIntSet(1,3,5,7) produced set with wrong cardinality`)
	}
	fmt.Println("TestNewIntSet:", v)
}

func TestCard(t *testing.T) {
	s := NewIntSet()
	if s.Card() != 0 {
		t.Error(`s.Card() != 0`)
	}
	v := NewIntSet(1, 3, 5, 7)
	if v.Card() != 4 {
		t.Error(`v.Card() != 4`)
	}
}

func TestUnion(t *testing.T) {
	s := NewIntSet(1, 3, 5, 7)
	v := NewIntSet(2, 4, 8)
	w := s.Union(v)
	if w.Card() != 7 {
		t.Error(`w.Card() != 7`)
	}
	fmt.Println("TestUnion: [w] = ", w)
	x := w.Union(NewIntSet(11, 12, 13))
	if x.Card() != 10 {
		t.Error(`x.Card() != 10`)
	}
	fmt.Println("TestUnion: [x] = ", x)
}

func TestDiff(t *testing.T) {
	s := NewIntSet(1, 3, 5, 7, 9, 11)
	v := NewIntSet(5, 7)
	w := s.Diff(v)
	if w.Card() != 4 {
		t.Error(`w.Card() != 4`)
	}
	fmt.Println("TestDiff: [w] = ", w)
	x := s.Diff(NewIntSet(9, 11))
	if x.Card() != 4 {
		t.Error(`x.Card() != 4`)
	}
	fmt.Println("TestDiff: [x] = ", x)
	y := s.Diff(v.Union(w))
	if y.Card() != 0 {
		t.Error(`y.Card() != 0`)
	}
	fmt.Println("TestDiff: [y] = ", y)
}

func TestInter(t *testing.T) {
	s := NewIntSet(1, 3, 5, 7, 9)
	v := s.Inter(NewIntSet(3, 5))
	if v.Card() != 2 {
		t.Error(`v.Card() != 2`)
	}
	fmt.Println("TestInter: [v] = ", v)
}

func TestSeq(t *testing.T) {
	s := NewIntSet(1, 3, 5, 7, 9)
	v := NewIntSet()
	if s.Seq().Length() != 5 {
		t.Error(`len(s.Seq()) != 5, but: `, s.Seq().Length())
	}
	fmt.Println("TestSeq: ", s.Seq().Elements())
	if v.Seq().Length() != 0 {
		t.Error(`len(v.Seq()) != 0`)
	}
	fmt.Println(s.Seq().Elements(), s.Seq().Tail().Elements(), s.Seq().Tail().Tail().Elements())
	fmt.Println(s.Seq().Head())
	fmt.Println(v.Seq().Head())
}
