package cspider

import "sort"

type IntSet struct {
	members map[int]bool
}

// NewIntSet returns a set containing the supplied integers.
func NewIntSet(nums ...int) *IntSet {
	rs := new(IntSet).init()
	for _, num := range nums {
		rs.members[num] = true
	}
	return rs
}

func (is *IntSet) init() *IntSet {
	is.members = make(map[int]bool)
	return is
}

// Card returns the cardinality of the receiver set.
func (is *IntSet) Card() int {
	return len(is.members)
}

// Add adds the members of the supplied set to the receiver.
func (is *IntSet) Add(addSet *IntSet) *IntSet {
	for k, _ := range addSet.members {
		is.members[k] = true
	}
	return is
}

// AddInt adds a single int to the receiver.
func (is *IntSet) AddInt(x int) *IntSet {
	is.members[x] = true
	return is
}

// AddRange adds the supplied inclusive range of values to the receiver.
func (is *IntSet) AddRange(sRange int, eRange int) *IntSet {
	for i := sRange; i <= eRange; i++ {
		is.members[i] = true
	}
	return is
}

// Diff returns a new IntSet formed by subtracting the supplied IntSet from
// the receiver set. It does not overwrite either.
func (is *IntSet) Diff(dSet *IntSet) *IntSet {
	rs := NewIntSet()
	for k, _ := range is.members {
		rs.members[k] = true
	}
	for k, _ := range dSet.members {
		delete(rs.members, k)
	}
	return rs
}

// Empty tests whether the receiver set is empty.
func (is *IntSet) Empty() bool {
	return is.Card() == 0
}

// Inter returns a new IntSet formed by the intersection of the receiver set
// and the supplied IntSet. It does not overwrite either.
func (is *IntSet) Inter(set *IntSet) *IntSet {
	rs := new(IntSet).init()
	for k, _ := range is.members {
		if set.members[k] {
			rs.members[k] = true
		}
	}
	return rs
}

// Member tests whether the supplied value is a member of the receiver set.
func (is *IntSet) Member(x int) bool {
	return is.members[x]
}

// Remove removes the supplied set of values from the receiver.
func (is *IntSet) Remove(remSet *IntSet) *IntSet {
	for k, _ := range remSet.members {
		if !is.members[k] {
			panic("element not found in map.")
		}
		delete(is.members, k)
	}
	return is
}

// Seq returns an IntSeq containing the members of the receiver set.
// Like the equivalent CSPM function, the iteration order is stable.
func (is *IntSet) Seq() *IntSeq {
	rsl := make([]int, len(is.members))
	i := 0
	for k, _ := range is.members {
		rsl[i] = k
		i++
	}
	sort.Ints(rsl)
	//fmt.Println("rsl: ", rsl)
	seq := NewIntSeq()
	for _, v := range rsl {
		seq.AddIntBack(v)
	}
	return seq
}

// Union returns a new IntSet formed from the union of the receiver set and the
// supplied IntSet. It does not overwrite either.
func (is *IntSet) Union(uSet *IntSet) *IntSet {
	rs := NewIntSet()
	for k, _ := range is.members {
		rs.members[k] = true
	}
	for k, _ := range uSet.members {
		rs.members[k] = true
	}
	return rs
}
