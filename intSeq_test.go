package cspider

import "testing"

func TestNewIntSeq(t *testing.T) {
	s := NewIntSeq()
	if s == nil {
		t.Error(`NewIntSeq() didn't return valid ptr`)
	}

	v := NewIntSeq(1, 3, 5, 7)
	if v == nil {
		t.Error(`NewIntSeq() didn't return valid ptr`)
	}
	if v.Length() != 4 {
		t.Error(`NewIntSeq(1,3,5,7) produced list with wrong length`)
	}
}

func TestLength(t *testing.T) {
	s := NewIntSeq()
	if s.Length() != 0 {
		t.Error(`NewIntSeq() returned nonzero length sequence`)
	}

	if NewIntSeq(1, 3, 5, 7).Length() != 4 {
		t.Error(`NewIntSeq(1,3,5,7) returned non-4 length sequence`)
	}
}

func TestAddBack(t *testing.T) {
	s := NewIntSeq(2, 4)
	s.AddBack(NewIntSeq(1, 3, 5))
	if s.Length() != 5 {
		t.Error(`s cardinality should be 5`)
	}
}

func TestAddFront(t *testing.T) {
	s := NewIntSeq(5, 6, 7)
	s.AddFront(NewIntSeq(1, 2, 3))
	if s.Length() != 6 {
		t.Error(`s.Length() should be 6`)
	}
	if s.Head() != 1 {
		t.Error(`s.Head should be 1`)
	}
}

func TestAddRangeFront(t *testing.T) {
	s := NewIntSeq(7, 10)
	s.AddRangeFront(4, 6)
	if s.Length() != 5 {
		t.Error(`s length should be 5`)
	}
	if s.Head() != 4 {
		t.Error(`s head should be 4`)
	}
}

func TestAddRangeBack(t *testing.T) {
	s := NewIntSeq(2, 4)
	s.AddRangeBack(7, 12)
	if s.Length() != 8 {
		t.Error(`s length should be 8`)
	}
}

func TestRemove(t *testing.T) {
	s := NewIntSeq(2, 4)
	s.Remove(NewIntSeq(2))
	if s.Length() != 1 {
		t.Error(`s cardinality should be 1`)
	}
	s.Remove(NewIntSeq(4))
	if s.Length() != 0 {
		t.Error(`s cardinality should be 0`)
	}
}

func TestHead(t *testing.T) {
	s := NewIntSeq(2, 4)
	if s.Head() != 2 {
		t.Error(`s head should be 2`)
	}
	s.Remove(NewIntSeq(2))
	if s.Head() != 4 {
		t.Error(`s.head should now be 4`)
	}
}

func TestTail(t *testing.T) {
	s := NewIntSeq(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
	if s.Tail().Length() != 9 {
		t.Error(`s.Tail().Length() should be 9`)
	}
	if s.Tail().Head() != 2 {
		t.Error(`s.Tail().Head() should be 2`)
	}
	//v := s.Tail().Tail()
	//fmt.Println(s.Elements())
	//fmt.Println(v.Elements())
}

func TestNull(t *testing.T) {
	s := NewIntSeq()
	if !s.Null() {
		t.Error(`s.Null() should be true`)
	}
	v := NewIntSeq(1)
	if v.Null() {
		t.Error(`v.Null() should be false`)
	}
}

func TestElem(t *testing.T) {
	s := NewIntSeq(1)
	if s.Elem(2) {
		t.Error(`s.Elem(2) should be false`)
	}
	if !s.Elem(1) {
		t.Error(`s.Elem(1) should be true`)
	}
}

func TestSet(t *testing.T) {
	s := NewIntSeq(1, 2, 3, 4)
	v := s.Set()
	for i := 1; i < 5; i++ {
		if !v.Member(i) {
			t.Error(`v[i] should be true`)
		}
	}
}
